# Prompts


**1. Please explain Big-O notation in simple terms.**

Big-O notation is the language used to describe an algorithm in terms of how its runtime grows relative to the input.
It can show the best, worst and average-case runtime of an algorithm, and can be used to compare the efficiency of different solutions.
With Big-O notation, the input is referred to as "n" and is used to express the speed at which the runtime grows.
For example, if a function runs in O(1) relative to its input, that means that no matter the size of the input, it only requires one step to run. It is constant and likely at its best or most efficient.
If a function runs in O(n^2), then the required steps to run grows exponentially with the size of the input. Therefore, it has a greater runtime than O(1) which is constant, relative to its input.


**2. What are the most important things to look for when reviewing another team member's code?**

An important thing to look for when reviewing code is to see if the code is doing what it's supposed to do, such as checking if features or bug fixes have been addressed.
Other things to look for are consistency with style guidelines, naming, etc. as well as comments, so the code is easier to read and understand.
It is also important to check for any hardcoded values and repeated code, as this can increase complexity and create room for more bugs.


**3. Describe a recent interaction with someone who was non-technical. What did you need to communicate and how did you do it?**

I needed to communicate to a production manager how to execute a specific action to a shot task on Ftrack. Since they were unfamiliar with the production tracker, I provided step by step instructions with images to outline what they needed to do to achieve their goal.
I provided some information on why it's important to have the correct task type selected, and reasons why the action might not work properly. I also let them know that they can easily contact me if they have any further questions or if they run into any issues.


**4. Implement a simple priority queue. Assume an incoming stream of dictionaries containing two keys; command to be executed and priority. Priority is an integer value [0, 10], where work items of the same priority are processed in the order they are received.**

https://gitlab.com/marvelyn.milan/personal/-/blob/main/priority_queue.py
