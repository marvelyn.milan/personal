"""
Priority Queue
This assumes an incoming stream of dictionaries containing two keys; command (to be executed) and priority
"""
from operator import itemgetter

queue = []


def add_to_queue(*args):
    """
    Adds dictionaries to the queue list
    Priority is set to 0 if it is missing from the dictionary
    :param args: dictionary containing command and priority keys
    :return: list of dictionaries
    """
    for item in args:
        if 'command' not in item:
            print('>>> Command missing.')
        if 'priority' not in item:
            print item
            item['priority'] = 0
        queue.append(item)

    return queue


def execute_commands():
    """
    Sorts the queue list by priority then executes the command
    Dictionary is deleted from the queue once the command is executed
    """
    if len(queue) > 1:
        sorted_queue = sorted(queue, key=itemgetter('priority'), reverse=True)
        for item in sorted_queue:
            exec (item['command'])
            queue.remove(item)
    else:
        exec (queue[0]['command'])
        del queue[0]
